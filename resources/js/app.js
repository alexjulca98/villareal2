
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

Vue.component('documentos-externos', require('./components/documentosexternos/DocumentosExternos.vue').default);
Vue.component('nuevo-docexterno', require('./components/documentosexternos/NuevoDoc.vue').default);
Vue.component('cargos-generados', require('./components/cargoentrega/Cargo.vue').default);
Vue.component('cargos-editar', require('./components/cargoentrega/CargosEditar.vue').default);
Vue.component('cargos-anular', require('./components/cargoentrega/CargosAnular.vue').default);
Vue.component('cargos-grabar', require('./components/cargoentrega/CargosGrabar.vue').default);
Vue.component('cargos-generar', require('./components/cargoentrega/CargoGenerar.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    data: {
        message: 'Hggg!'
      }
});
